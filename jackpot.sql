-- phpMyAdmin SQL Dump
-- version 4.2.12deb2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Czas generowania: 11 Kwi 2016, 22:08
-- Wersja serwera: 5.5.43-0+deb8u1
-- Wersja PHP: 5.6.9-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Baza danych: `jackpot`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `chat_badwords`
--

CREATE TABLE IF NOT EXISTS `chat_badwords` (
`id` int(11) NOT NULL,
  `word` varchar(50) NOT NULL,
  `used` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `chat_badwords`
--

INSERT INTO `chat_badwords` (`id`, `word`, `used`) VALUES
(1, 'kurwa', 0),
(2, 'fuck', 0),
(3, 'fuck', 0),
(4, 'shit', 0),
(5, 'asshole', 0),
(6, 'cunt', 0),
(7, 'fag', 0),
(8, 'fuk', 0),
(9, 'fck', 0),
(10, 'fcuk', 0),
(11, 'assfuck', 0),
(12, 'assfucker', 0),
(13, 'fucker', 0),
(14, 'motherfucker', 0),
(15, 'asscock', 0),
(16, 'asshead', 0),
(17, 'asslicker', 0),
(18, 'asslick', 0),
(19, 'assnigger', 0),
(20, 'nigger', 0),
(21, 'asssucker', 0),
(22, 'bastard', 0),
(23, 'bitch', 0),
(24, 'bitchtits', 0),
(25, 'bitches', 0),
(26, 'bitch', 0),
(27, 'brotherfucker', 0),
(28, 'bullshit', 0),
(29, 'bumblefuck', 0),
(30, 'buttfucka', 0),
(31, 'fucka', 0),
(32, 'buttfucker', 0),
(33, 'buttfucka', 0),
(34, 'fagbag', 0),
(35, 'fagfucker', 0),
(36, 'faggit', 0),
(37, 'faggot', 0),
(38, 'faggotcock', 0),
(39, 'fagtard', 0),
(40, 'fatass', 0),
(41, 'fuckoff', 0),
(42, 'fuckstick', 0),
(43, 'fucktard', 0),
(44, 'fuckwad', 0),
(45, 'fuckwit', 0),
(46, 'dick', 0),
(47, 'dickfuck', 0),
(48, 'dickhead', 0),
(49, 'dickjuice', 0),
(50, 'dickmilk', 0),
(51, 'doochbag', 0),
(52, 'douchebag', 0),
(53, 'douche', 0),
(54, 'dickweed', 0),
(55, 'dyke', 0),
(56, 'dumbass', 0),
(57, 'dumass', 0),
(58, 'fuckboy', 0),
(59, 'fuckbag', 0),
(60, 'gayass', 0),
(61, 'gayfuck', 0),
(62, 'gaylord', 0),
(63, 'gaytard', 0),
(64, 'nigga', 0),
(65, 'niggers', 0),
(66, 'niglet', 0),
(67, 'paki', 0),
(68, 'piss', 0),
(69, 'prick', 0),
(70, 'pussy', 0),
(71, 'poontang', 0),
(72, 'poonany', 0),
(73, 'porchmonkey', 0),
(74, 'porch monkey', 0),
(75, 'poon', 0),
(76, 'queer', 0),
(77, 'queerbait', 0),
(78, 'queerhole', 0),
(79, 'queef', 0),
(80, 'renob', 0),
(81, 'rimjob', 0),
(82, 'ruski', 0),
(83, 'sandnigger', 0),
(84, 'sand nigger', 0),
(85, 'schlong', 0),
(86, 'shitass', 0),
(87, 'shitbag', 0),
(88, 'shitbagger', 0),
(89, 'shitbreath', 0),
(90, 'chinc', 0),
(91, 'carpetmuncher', 0),
(92, 'chink', 0),
(93, 'choad', 0),
(94, 'clitface', 0),
(95, 'clusterfuck', 0),
(96, 'cockass', 0),
(97, 'cockbite', 0),
(98, 'cockface', 0),
(99, 'skank', 0),
(100, 'skeet', 0),
(101, 'skullfuck', 0),
(102, 'slut', 0),
(103, 'slutbag', 0),
(104, 'splooge', 0),
(105, 'twatlips', 0),
(106, 'twat', 0),
(107, 'twats', 0),
(108, 'twatwaffle', 0),
(109, 'vaj', 0),
(110, 'vajayjay', 0),
(111, 'va-j-j', 0),
(112, 'wank', 0),
(113, 'wankjob', 0),
(114, 'wetback', 0),
(115, 'whore', 0),
(116, 'whorebag', 0),
(117, 'whoreface', 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `games`
--

CREATE TABLE IF NOT EXISTS `games` (
  `id` int(11) NOT NULL DEFAULT '0',
  `starttime` int(11) NOT NULL,
  `cost` text,
  `winner` varchar(128) NOT NULL,
  `userid` varchar(70) NOT NULL,
  `percent` varchar(10) DEFAULT NULL,
  `itemsnum` int(11) NOT NULL,
  `module` text NOT NULL,
  `players` int(2) NOT NULL,
  `cheat_win` tinyint(1) DEFAULT '0',
  `stating` varchar(11) NOT NULL DEFAULT 'wait'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `games_info`
--

CREATE TABLE IF NOT EXISTS `games_info` (
`id` int(11) NOT NULL,
  `userid` varchar(70) NOT NULL,
  `username` varchar(70) NOT NULL,
  `item` text,
  `color` text,
  `value` text,
  `avatar` varchar(512) NOT NULL,
  `image` text NOT NULL,
  `from` text NOT NULL,
  `to` text NOT NULL,
  `game_id` int(50) unsigned NOT NULL DEFAULT '17'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `info`
--

CREATE TABLE IF NOT EXISTS `info` (
  `name` varchar(255) NOT NULL,
  `desc` varchar(100) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `info`
--

INSERT INTO `info` (`name`, `desc`, `value`) VALUES
('current_game', 'Current game', '1'),
('state', 'Current game state', 'waiting'),
('rake', 'Rake percent', '10'),
('minbet', 'Minimum bet', '0'),
('maxitems', 'Max user items', '15'),
('maxbet', 'Maximum bet (if > start round)', '1000000'),
('maxritem', 'Max round items (if > start round)', '100'),
('bot_trade_id', '$_GET[‘partner’] from bot account tradelink', ''),
('bot_trade_token', '$_GET[token] frm bot account tradelink', ''),
('steamauth_apik', 'APIKEY -site auth', ''),
('steam_apik_domain', 'Domain generated APIKEY-site', 'jackpot1.pl'),
('bot_admin_steamid', 'BOT Admin steamID(for getting messages from bot)', ''),
('bot_bot_steamid', 'BOT account steamID', ''),
('bot_sharedsecret', 'BOT account sharedsecret [for login]', ''),
('bot_identitysecret', 'BOT account identitysecret [for login]', ''),
('bot_bot_apikey', 'BOT account APIKEY', ''),
('bot_domain', 'BOT site URL (for catalog: /system)', 'jackpot1.pl'),
('bot_gametime', 'BOT Gametime (s)', '20'),
('bot_account_name', 'BOT username [for login]', 'YOURUSERNAME'),
('bot_account_password', 'BOT password (alphanumeric upper/lower) [for login] (hashed in db [sha1])', '94589c45381fc388e1ffdc208a97fb41'),
('bot_appid', 'BOT <a href="https://developer.valvesoftware.com/wiki/Steam_Application_IDs">Steam game appid</a>', '730'),
('rakestring_username', 'What must be in user username to get more percent (rake string)', 'jackpot1.pl'),
('bot_minrplayers', 'BOT Minimum players to start round (>=2)', '2'),
('increse_chance_percent', 'Percent which incres player to win if have rakestring_username', '5');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `items`
--

CREATE TABLE IF NOT EXISTS `items` (
`id` int(11) NOT NULL,
  `name` text NOT NULL,
  `cost` text NOT NULL,
  `lastupdate` text NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=245 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `items`
--

INSERT INTO `items` (`id`, `name`, `cost`, `lastupdate`) VALUES
(184, 'SSG%2008%20|%20Blue%20Spruce%20(Battle-Scarred)', '0.04', '1460404166'),
(243, 'Nova%20|%20Sand%20Dune%20(Field-Tested)', '0.04', '1460401944'),
(186, 'Tec-9%20|%20Army%20Mesh%20(Field-Tested)', '0.04', '1460404166'),
(187, 'AK-47%20|%20Safari%20Mesh%20(Field-Tested)', '0.13', '1460404167'),
(188, 'SCAR-20%20|%20Contractor%20(Field-Tested)', '0.04', '1460404167'),
(189, 'Five-SeveN%20|%20Forest%20Night%20(Field-Tested)', '0.04', '1460404167'),
(190, 'PP-Bizon%20|%20Night%20Ops%20(Well-Worn)', '0.06', '1460404168'),
(191, 'MP9%20|%20Storm%20(Field-Tested)', '0.05', '1460404169'),
(192, 'M4A1-S%20|%20Boreal%20Forest%20(Field-Tested)', '0.19', '1460404169'),
(193, 'USP-S%20|%20Royal%20Blue%20(Field-Tested)', '0.53', '1460404169'),
(194, 'Tec-9%20|%20Army%20Mesh%20(Minimal%20Wear)', '0.04', '1460404170'),
(195, 'G3SG1%20|%20Polar%20Camo%20(Minimal%20Wear)', '0.04', '1460404170'),
(196, 'SSG%2008%20|%20Blue%20Spruce%20(Field-Tested)', '0.04', '1460404170'),
(197, 'P250%20|%20Boreal%20Forest%20(Battle-Scarred)', '0.04', '1460404171'),
(198, 'UMP-45%20|%20Corporal%20(Minimal%20Wear)', '0.11', '1460404171'),
(199, 'SSG%2008%20|%20Abyss%20(Field-Tested)', '0.11', '1460404171'),
(240, 'Operation%20Phoenix%20Weapon%20Case', '0.14', '1460396915'),
(201, 'Chroma%20Case', '0.11', '1460404171'),
(238, 'Chroma%202%20Case', '0.17', '1460396536'),
(203, 'MP9%20|%20Sand%20Dashed%20(Battle-Scarred)', '0.04', '1460404172'),
(237, 'Operation%20Breakout%20Weapon%20Case', '0.04', '1460396536'),
(205, 'Falchion%20Case', '0.05', '1460404172'),
(236, 'Sticker%20|%20Virtus.Pro%20|%20Cologne%202015', '0.41', '1460394441'),
(232, 'Sticker%20|%20Fnatic%20|%20Cluj-Napoca%202015', '0.43', '1460404172'),
(208, 'P250%20|%20Mint%20Kimono%20(Minimal%20Wear)', '0.18', '1460404223'),
(233, 'Sticker%20|%20NEO%20|%20Cologne%202015', '0.51', '1460161338'),
(234, 'Sticker%20|%20pronax%20|%20Cluj-Napoca%202015', '0.30', '1460394440'),
(220, 'Revolver%20Case', '0.04', '1460404238'),
(212, 'AUG%20|%20Storm%20(Field-Tested)', '0.04', '1460404238'),
(213, 'G3SG1%20|%20Polar%20Camo%20(Field-Tested)', '0.04', '1460404239'),
(244, 'Sawed-Off%20|%20Sage%20Spray%20(Minimal%20Wear)', '0.05', '1460402447'),
(215, 'MP7%20|%20Forest%20DDPAT%20(Field-Tested)', '0.04', '1460404240'),
(221, 'MAC-10%20|%20Silver%20(Factory%20New)', '0.07', '1460404240'),
(231, 'XM1014%20|%20Quicksilver%20(Factory%20New)', '0.16', '1460404240'),
(218, 'MP9%20|%20Sand%20Dashed%20(Field-Tested)', '0.04', '1460404240'),
(219, 'Shadow%20Case', '0.05', '1460404241'),
(222, 'XM1014%20|%20Blue%20Spruce%20(Field-Tested)', '0.04', '1460404242'),
(223, 'MP7%20|%20Army%20Recon%20(Field-Tested)', '0.04', '1460404225'),
(224, 'Nova%20|%20Predator%20(Field-Tested)', '0.04', '1460404226'),
(225, 'StatTrak™%20AWP%20|%20Worm%20God%20(Well-Worn)', '5.92', '1460404226'),
(226, 'Operation%20Wildfire%20Case', '0.17', '1460404226'),
(227, 'Sticker%20|%20Titan%20|%20Cologne%202015', '0.80', '1460404227'),
(228, 'AK-47%20|%20Blue%20Laminate%20(Field-Tested)', '2.18', '1460404227'),
(241, 'Dual%20Berettas%20|%20Stained%20(Field-Tested)', '0.06', '1460397398'),
(242, 'Galil%20AR%20|%20Rocket%20Pop%20(Field-Tested)', '0.11', '1460397398');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
`id` int(11) NOT NULL,
  `userid` varchar(70) NOT NULL,
  `msg` text NOT NULL,
  `from` text NOT NULL,
  `win` int(10) NOT NULL,
  `system` int(10) NOT NULL,
  `time` int(20) NOT NULL,
  `used` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `queue`
--

CREATE TABLE IF NOT EXISTS `queue` (
`id` int(11) NOT NULL,
  `userid` varchar(70) NOT NULL,
  `token` varchar(128) NOT NULL,
  `items` text NOT NULL,
  `gamenum` int(11) NOT NULL,
  `status` varchar(45) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rakeitems`
--

CREATE TABLE IF NOT EXISTS `rakeitems` (
`id` int(11) NOT NULL,
  `gamenum` int(11) DEFAULT NULL,
  `item` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `steamid` varchar(70) NOT NULL,
  `tlink` varchar(255) DEFAULT NULL,
  `won` float DEFAULT '0',
  `admin` int(11) NOT NULL,
  `chat_admin` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL,
  `avatar` varchar(255) NOT NULL,
  `games` int(11) NOT NULL,
  `ban` int(2) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id`, `steamid`, `tlink`, `won`, `admin`, `chat_admin`, `name`, `avatar`, `games`, `ban`) VALUES
(37, '76561198026196676', 'https://steamcommunity.com/tradeoffer/new/?partner=65930948&token=fTXSrvfx', 7.62, 1, 1, 'Ufogufo13', 'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/6f/6f886a97b8f387bf16f4bbbc392778a2eae28bac_full.jpg', 5, 0);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `chat_badwords`
--
ALTER TABLE `chat_badwords`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `games`
--
ALTER TABLE `games`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `games_info`
--
ALTER TABLE `games_info`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `info`
--
ALTER TABLE `info`
 ADD PRIMARY KEY (`name`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `queue`
--
ALTER TABLE `queue`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rakeitems`
--
ALTER TABLE `rakeitems`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `chat_badwords`
--
ALTER TABLE `chat_badwords`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=118;
--
-- AUTO_INCREMENT dla tabeli `games_info`
--
ALTER TABLE `games_info`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `items`
--
ALTER TABLE `items`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=245;
--
-- AUTO_INCREMENT dla tabeli `messages`
--
ALTER TABLE `messages`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `queue`
--
ALTER TABLE `queue`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `rakeitems`
--
ALTER TABLE `rakeitems`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=42;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
