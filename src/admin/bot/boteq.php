<html lang="en">
<head>
<link rel="stylesheet" href="/assets/css/colorbox.css" />
   <META content="text/html; charset=utf-8" http-equiv="Content-Type">
	<title><?php echo $title ?></title>
	<link rel="stylesheet" href="/assets/css/style.css">
	<link rel="stylesheet" href="/assets/css/simple-line-icons.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script type="text/javascript" src="/assets/js/noty/packaged/jquery.noty.packaged.min.js"></script>
	<script src="/assets/js/progressbar.js"></script>
	<script src="/assets/js/main.js"></script>
</head>
<body>
<div class="content">
    <div class="jackpot">
		<div align="center">
            <div class="stock">
                <div style="color: #ffc600" class="title-stock">Bot EQ</div>
                <div class="content-stock">
                    <div align="center">
                        <?php 
                        @include_once $_SERVER['DOCUMENT_ROOT'].('/set.php');
                        $botsteamid = fetchinfo("value","info","name","bot_bot_steamid");
                        $steamappid = fetchinfo("value","info","name","bot_appid");

                        function getInventory($steamid,$steamappid) {

                            $url = "http://steamcommunity.com/profiles/".$steamid;
                            $json_object = file_get_contents($url);
                            $json_decoded = json_decode($json_object);

                            $str = $json_decoded->response->players[0]->steamid;

                            $urlInv = "http://steamcommunity.com/profiles/".$steamid. $str . "/inventory/json/".$steamappid."/2/?trading=2";

                            $json_object_inv = file_get_contents($urlInv);
                            $json_decoded_inv = json_decode($json_object_inv, true);

                            // iterate through Inventory and find ids.
                            $rgInventory = $json_decoded_inv[rgInventory];
                            $rgInventory = array_values($rgInventory);

                            $rgDesc = $json_decoded_inv[rgDescriptions];
                            $rgDesc = array_values($rgDesc);

                            $itemnames = array();

                            for ($i = 0; $i < count($rgInventory); $i++) {  //iterate through rgInventory.
                                $classidInv = $rgInventory[$i]['classid'];
                                $instanceidInv = $rgInventory[$i]['instanceid'];

                                for ($j = 0; $j < count($rgDesc); $j++) {   //iterate through rgDesc.
                                    $classid = $rgDesc[$j]['classid'];
                                    $instanceid = $rgDesc[$j]['instanceid'];

                                    if ($classidInv == $classid && $instanceidInv == $instanceid) {
                                        $icon_url = $rgDesc[$j]['icon_url'];
                                        $market_name = $rgDesc[$j]['market_name'];
                                        $market_name_formatted = str_replace(" ", "%20", $market_name);
                                        $st_color = $rgDesc[$j]['name_color'];
                                        @$itemnames = array_push($itemnames, $market_name_formatted);



                                        for ($k = 0; $k < count($rgDesc[$j]['tags']); $k++) {
                                            if ($rgDesc[$j]['tags'][$k]['category'] == "Rarity") {
                                                $name_color = $rgDesc[$j]['tags'][$k]['color'];
                                            } else if ($rgDesc[$j]['tags'][$k]['category'] == "Exterior") {

                                                $name_exterior = $rgDesc[$j]['tags'][$k]['name'];
                                            }
                                        }
                                    }
                                }

                                if ($st_color == "CF6A32") {
                                    $name_color = "CF6A32";
                                }
                                    $market_names = str_replace(" ", '%20', $market_name);
                                $link = "http://steamcommunity.com/market/priceoverview/?country=EN&currency=1&appid=".$steamappid."&market_hash_name=".$market_names."";
                                $string = file_get_contents($link);
                                $obj = json_decode($string);
                                $result = substr(strstr($string, '$'), 1, strlen($string));
                                $a = explode('$', $result);  
                                $a = $a[1]; 
                                $a = str_replace('"}','', $a);
                                $lowest_price = (float)($a);
                                $lowest_prices = $lowest_price;
                                echo '<div class="divinv" style="border: 2px solid #' . $name_color . '" data-toggle="tooltip" data-placement="bottom" title="'.$market_name.'"><img id="stay" class="weaponinv"  src="http://steamcommunity-a.akamaihd.net/economy/image/' . $icon_url . '" width="80px" height="60px" alt="' . $market_name.'"/><div class="rarityinv">' . $lowest_prices . ' $</div></div>';

                                $name_exterior = null;
                            }

                            priceInv($itemnames);
                        }



                        echo @getInventory($botsteamid,$steamappid);
                         ?>  
                    </div>
                </div>
            </div>
        </div>
    </div>												
</div>
</body>
</html>
