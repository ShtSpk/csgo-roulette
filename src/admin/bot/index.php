<?php 
@include_once $_SERVER['DOCUMENT_ROOT'].('/set.php');
require $_SERVER['DOCUMENT_ROOT'].('/steamauth/steamauth.php');

if(isset($_SESSION["steamid"])) {
    @include_once $_SERVER['DOCUMENT_ROOT'].('/set.php');
    @include_once $_SERVER['DOCUMENT_ROOT'].('/steamauth/steamauth.php');
    @include_once $_SERVER['DOCUMENT_ROOT'].('/steamauth/userInfo.php');
    $login = $_SESSION["steamid"];
    $sql = "SELECT * FROM `users` WHERE `steamid` LIKE '$login'";
    $result = mysql_query($sql);
    $row = mysql_fetch_object($result);
    if ($row->admin !== '1') {
        echo 'You are not admin!' ; 
    }else{ ?>

        <html class="">
        <head>
            <title>Admin page</title>
            <meta name="robots" content="noindex, nofollow">
            <meta charset="UTF-8">
                  
            <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=all" rel="stylesheet" type="text/css">
            <link href="/admin/style/login/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
            <link href="/assets/css/simple-line-icons.css" rel="stylesheet" type="text/css">
            <link href="/admin/style/login/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
            <link href="/admin/style/login/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
            <link href="/admin/style/login/pages/css/login.css" rel="stylesheet" type="text/css">
            <link href="/admin/style/login/global/css/components.css" id="style_components" rel="stylesheet" type="text/css">
            <link href="/admin/style/login/global/css/plugins.css" rel="stylesheet" type="text/css">
            <link href="/admin/style/login/layout/css/layout.css" rel="stylesheet" type="text/css">
            <link href="/admin/style/login/layout/css/themes/darkblue.css" rel="stylesheet" type="text/css" id="style_color">
            <link href="/admin/style/login/layout/css/custom.css" rel="stylesheet" type="text/css">
            <link rel="shortcut icon" href="favicon.ico">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        </head>
        <body>

        <div class="menu-toggler sidebar-toggler">
        </div>

        <div class="logo">
            <a href="index.php">
            <img src="" alt="WINYOURSKINS">
            </a>
        </div>
        
        <?php

        function iEncrypt($data, $key, $iv) {
            $blocksize = 16;
            $pad = $blocksize - (strlen($data) % $blocksize);
            $data = $data . str_repeat(chr($pad), $pad);
            return bin2hex(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $data, MCRYPT_MODE_CBC, $iv));
        }

        function iDecrypt($data, $key, $iv) {
            return mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, hex2bin($data), MCRYPT_MODE_CBC, $iv);
        }

        if(isset($_POST['do']) && $_POST['do']=='Update') {
            $i=0;
            $ee=0;
            foreach($_POST AS $key => $value){

                if((empty($value) || !isset($value)) && $value!=0){
                    $mess= '<div class="alert alert-danger" style="background-color:red;">insert all fields</div>'.$value;
                    $ee=1;
                    break;
                }
                if($key=="bot_account_password")
                    $value=iEncrypt(trim($value), $AESkey, $CRYPTiv);

                $ins = mysql_query("UPDATE info SET `value`='$value' WHERE `name`='$key'");
                if(!$ins)
                    $i++;
            }
            if($ee!=1){
                if($i==0)
                    $mess= '<div class="alert alert-success" style="background-color:lime;">updated</div>';
                else
                    $mess= '<div class="alert alert-danger" style="background-color:red;">error db</div>';
            }
        }
        
        include $_SERVER['DOCUMENT_ROOT'].'/admin/components/head.php';
        ?>
        <div class="panel panel-default">
            <div class="panel-heading"><h6 class="panel-title">OPTIONS</h6></div>
            <div class="panel-body">
                <div class="alert alert-info">
                    to change <strong style='color:red;'>bot</strong> options send form and restart bot, powinno nie spierdolic trwajacej gry
                </div>
                <?php if(isset($mess)) echo $mess; ?>
                <form action="#" method="POST" role="form">
                    <?php
                    $rs = mysql_query("SELECT * FROM info");
                        while($row = mysql_fetch_array($rs)) { ?>
                        <div class="form-group">
                            <label <?php if(strstr($row['desc'], 'bot_')) echo 'style="color:red;"'; ?>for="<?php echo $row['name']; ?>"><?php echo $row['desc']; ?></label>
                            <input type="text" clas="push-right" name="<?php echo $row['name']; ?>" value="<?php if($row['name']=='bot_account_password') echo iDecrypt(trim($row['value']), $AESkey, $CRYPTiv); else echo $row['value']; ?>" <?php if($row['name']=='current_game' || $row['name']=='state') echo 'disabled'; ?>></br>
                        </div>
                    <?php } ?>
                    <input type="submit" class="btn btn-primary" name="do" value="Update" />
                </form>						
            </div>
        </div>
    <div class="footer">
        <a href="mailto:ufogufo13@gmail.com" title="">ufogufo13@gmail.com</a>
    </div>

</body>
</html>
<?php }

}else{
    echo 'You are not logged!' ;    
}
?>