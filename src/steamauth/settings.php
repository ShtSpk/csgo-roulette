<?php
@include_once('../set.php');
$steamauth_apik = fetchinfo("value","info","name","steamauth_apik");
$steam_apik_domain = fetchinfo("value","info","name","steam_apik_domain");

$steamauth['apikey'] = $steamauth_apik; // Your Steam WebAPI-Key found at http://steamcommunity.com/dev/apikey
$steamauth['domainname'] = $steam_apik_domain; // The main URL of your website displayed in the login page
$steamauth['logoutpage'] = ""; // Page to redirect to after a successfull logout (from the directory the SteamAuth-folder is located in) - NO slash at the beginning!
$steamauth['loginpage'] = "/"; // Page to redirect to after a successfull login (from the directory the SteamAuth-folder is located in) - NO slash at the beginning!
?>
