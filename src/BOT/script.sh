#!/bin/bash

# ufogufo13@gmail.com
SCRIPTDIR=$(dirname "$(readlink -e "$0")")
DIR="/home/www/jackpot1.pl/BOT"
EXEC='sell.js'
SCREENNAME="jackpot1"
DESC="jackpot1"

#WARNING KURWA
#to work need node.js directory

#command: 'which node'

#paste there \/

NODE_DIRECTORY="/home/ufogufo13/.nvm/versions/node/v4.1.1/bin/node"
INVOCATION="$NODE_DIRECTORY $DIR/$EXEC"
USERNAME='ufogufo13'
ME=`whoami`

as_user() {
        if [ $ME == $USERNAME ] ; then
                bash -c "$1"
        else
                su $USERNAME -s /bin/bash -c "$1"
        fi
}

check_permissions() {
	pidfile=${DIR}/${SCREENNAME}.pid
        as_user "touch $pidfile"
        if ! as_user "test -w '$pidfile'" ; then 
                echo "Check Permissions. Cannot write to $pidfile. Correct the permissions and then excute: $0 status"
        fi
}

is_running(){
	pidfile=${DIR}/${SCREENNAME}.pid
	if [ -r "$pidfile" ]
        then
                pid=$(head -1 $pidfile)
                if ps ax | grep -v grep | grep ${pid} | grep "${SCREENNAME}" > /dev/null
                then
                        return 0
                else
                        return 1
                fi
        else
                if ps ax | grep -v grep | grep "${SCREENNAME} ${INVOCATION}" > /dev/null
                then
                        echo "No pidfile found, but server's running."
                        echo "Re-creating the pidfile."
			pid=$(ps ax | grep -v grep | grep "${SCREENNAME} ${INVOCATION}" | awk '{print $1}')
			check_permissions
			as_user "$pid > $pidfile"
                        return 0
                else
                        return 1
                fi
        fi
}

function startServer {
	if [ -d $DIR ]; then
		if is_running
		then
			echo "$DESC is running! Will not start server."
		else
			check_permissions
			pidfile=${DIR}/${SCREENNAME}.pid
			if [ -f $DIR/$EXEC ]; then
				as_user "screen -dmS $SCREENNAME $INVOCATION"
				echo "$DESC started !"
				echo "Started"
				as_user "screen -list | grep '\.$SCREENNAME' | cut -f1 -d'.' | tr -d -c 0-9 > $pidfile"
			else
				as_user "ls"
				echo " Error: sexecutable file not found !"
			fi
		fi

	else
		echo "Error: directory not found !"
	fi

}

function stopServer {
	if is_running
        then
		check_permissions
		as_user "kill $pid"
		as_user "rm $pidfile"
                echo "$DESC stopped !"
		echo "Stopped"
	else
		echo "$DESC is currently not running."
	fi
}

function serverStatus {
	if is_running
	then
		echo "$DESC is currently running."
		echo "1"
	else
		echo "$DESC is currently not running."
		echo "0"
	fi
}

case "$1" in
	start)
		startServer
		;;

	stop)
		stopServer
		;;

	restart)
		stopServer
		sleep 1
		startServer
		;;

	status)
		serverStatus
		;;


	*)
		echo "Usage: $0 {start|stop|restart|status}"
		exit 1
		;;
esac
exit
