<?php
ini_set('display_errors', 1);
require_once "langdoc.php";

require_once('set.php');

require_once('steamauth/steamauth.php');
require_once('steamauth/userInfo.php');

require_once ('chat/adminlist.php');
require_once ('chat/chatfunctions.php');

$sitename = fetchinfo("value","info","name","steam_apik_domain");

$lastgame = fetchinfo("value","info","name","current_game");
$bot_trade_id = fetchinfo("value","info","name","bot_trade_id");
$bot_trade_token = fetchinfo("value","info","name","bot_trade_token");
$lastwinner = fetchinfo("userid","games","id",$lastgame-1);
$winnercost = fetchinfo("cost","games","id",$lastgame-1);
$winnerpercent = round(fetchinfo("percent","games","id",$lastgame-1),1);
$winneravatar = fetchinfo("avatar","users","steamid",$lastwinner);
$winnername = fetchinfo("name","users","steamid",$lastwinner);

if(isset($_SESSION["steamid"])){
 $igr = fetchinfo("games","users","steamid",$_SESSION["steamid"]);
 $deneg = fetchinfo("won","users","steamid",$_SESSION["steamid"]);
}

?>

<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="steam jackpot system">
  <meta name="author" content="ufogufo13@gmail.com">

  <title>jackpot</title>

  <link rel="stylesheet" href="./assets/normalize.css">
  <link rel="stylesheet" href="./assets/css.css">
  <link rel="stylesheet" href="./assets/jackpot.css">
  <link rel="stylesheet" href="/assets/css/navbar-static-top.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script>var LOTS = MY_LOTS = FULL_LOT = false; </script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script type="text/javascript" src="assets/js/noty/packaged/jquery.noty.packaged.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
  <script src="/assets/js/ie10-viewport-bug-workaround.js"></script>
  <script src="assets/js/progressbar.js"></script>
  <script src="assets/js/main.js"></script>
  <script src="assets/js/script.js"></script>
  <script src="chat/chat.js"></script>

  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <script>
  function load_messes(){
    $.ajax({
      type: "POST",
      url:  "chat/chatread.php",
      data: "req=ok",
      success: function(test){
        $("#mcaht").empty();
        $("#mcaht").append(test);
        $("#mcaht").scrollTop();
      }
    });
  }

  function titleUpdate(){
    $.ajax({
      type: "POST",
      url:  "ajax/getcurrentgamecost.php",
      data: "",
      success: function(test){
        $("title").empty();
        $("title").append(test);
      }
    });
  }

  titleUpdate();
  setInterval(titleUpdate,5000);
  </script>

</head>

<body class="cover" style="overflow: visible; margin-right: 0px;">
  <div class="result success" id="success"></div>
  <div class="result error" id="error"></div>
  <nav class="navbar navbar-default navbar-static-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Jackpot system</a>
      </div>
      <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
          <li class="active"><a href="/index.php"><?php echo $msg[$lang]["play"] ;?></a></li>
          <li><a href="/history.php"><?php echo $msg[$lang]["history"] ;?></a></li>
          <li><a href="/top.php"><?php echo $msg[$lang]["top"] ;?></a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
        <?php if(!isset($_SESSION["steamid"])) { ?>
          <li><a href="?login"><?php echo $msg[$lang]['authwst']; ?></a></li>
        <?php } else{ ?>
          <li><img class="avatarimage" src="<?php echo $steamprofile['avatar']; ?>"></li>
          <li><a href="steamauth/logout.php"><?php echo $msg[$lang]["logout"]; ?></a></li>
        <?php } ?>
        </ul>
      </div><!--/.nav-collapse -->
    </div>
  </nav>
  <div class="container">
    <div class="jumbotron">
      <div class="row">
      <?php 
      if(!isset($_SESSION["steamid"])) {
        steamlogin();
        $rakestring_username=fetchinfo("value","info","name","rakestring_username");
        ?>
        <div class="col-md-4">
          <h4>Some info:</h4>
          <div>
            <ul>
              <li>Minimum bet: <?php echo fetchinfo("value","info","name","minbet"); ?>$</li>
              <li>Max items in round per player: <?php echo fetchinfo("value","info","name","maxitems"); ?></li>
              <li>Max items per round: <?php echo fetchinfo("value","info","name","maxritem"); ?></li>
              <li>Round time: <?php echo fetchinfo("value","info","name","bot_gametime"); ?>s</li>
              <li>+5% for winner: <?php echo $rakestring_username; ?> (eg. in username: johnsmith <b><?php echo $rakestring_username; ?></b> )</li>
            </ul>
          </div>
        </div>

      <?php } else { 
        $rakestring_username=fetchinfo("value","info","name","rakestring_username");
        mysql_query("UPDATE users SET name='".$steamprofile['personaname']."', avatar='".$steamprofile['avatarfull']."' WHERE steamid='".$_SESSION["steamid"]."'");
      ?>
      
        <div class="col-md-4 some_info">
          <h4>Some info:</h4>
          <div class="input-group">      
                  <form method="POST" action="ajax/updatelink.php">                    
                    <input type="text" name="link" id="link" placeholder="<?php echo $msg[$lang]["inserttrlink"]; ?>" value="<?php echo fetchinfo("tlink","users","steamid",$_SESSION["steamid"]); ?>">
                    <button type="submit" class="input-group-save-link">OK</button>

                    <a href="http://steamcommunity.com/id/me/tradeoffers/privacy#trade_offer_access_url" target="_blank">
                    <button type="button" class="input-group-getlink">?</button>
                    </a>
                  </form>
                </div>
          <div>
            <ul>
              <li><?php echo $msg[$lang]["winnum"]; ?>: <span><?php echo $igr; ?></li>
              <li><?php echo $msg[$lang]["winamount"]; ?> $<span><?php echo $deneg; ?></li>
              <li><?php echo $msg[$lang]["lastwinner"]; echo $msg[$lang]["nick"].': '.$winnername.' | '.$msg[$lang]["chance"].': '.$winnerpercent.'% | '.$msg[$lang]["winamount"].': '.round($winnercost,2); ?>$</li>
            </ul>
          </div>

        </div>
      <?php } ?>
        <div class="col-md-4">
          <h4>Play:</h4>
          <div class="progres">
            <div class="visual">
              <div id="prograsd">
                <p class="progressbar__label" style="position: absolute; top: 45%; left: 50%; padding: 0px; margin: 0px; transform: translate(-50%, -50%); color: #333;font: 500 40px/40px roboto,sans-serif;"><?php echo $msg[$lang]["loading"]; ?></p>
                <span id="money_round" class="bankbbas">
                  <span id="bank"><?php echo round(fetchinfo("cost","games","id",$lastgame),2); ?>
                  </span>$
                </span>
              </div>
            </div>
            <div class="under_circle">
              <div class="amount">
                <span id="end_game1" class="end_game"><?php echo $msg[$lang]["timeleft"]; ?>: <i class="icon-clock"></i> <span id="timeleft"><?php echo $msg[$lang]["loading"]; ?></span></span>
                <span id="end_game2" class="end_game"></span>
              </div>

              <?php
              if(!isset($_SESSION["steamid"])) 
                echo '<div id="add_game"><a href="?login">'.$msg[$lang]["play"].'!</a></div>';
              else {
                $token = fetchinfo("tlink","users","steamid",$_SESSION["steamid"]);
                if(strlen($token) < 2) 
                  echo '<div id="add_game"><a href="#" onclick="alert2(\''.$msg[$lang]["tradelinkmustbeinsert"].'!\')">'.$msg[$lang]["play"].'</a></div>';
                else 
                  echo '<div id="add_game"><a href="https://steamcommunity.com/tradeoffer/new/?partner='.$bot_trade_id.'&token='.$bot_trade_token.'" target=_blank>'.$msg[$lang]["play"].'</a></div>';
              } ?>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <h4>Chat:</h4>
          <div class="chat"> 
            <div class="chat-container">
              <div class="chat-buttons"> 
                <div class="media-body"> 
                  <div class="input-group"> 
                    <input type="text" class="form-control chat-message" id="text-massage" style="margin-left:6px;" maxlength="300"> 
                    <span class="input-group-btn"> 
                      <?php if(isset($_SESSION['steamid'])) { ?>
                      <button class="btn btn-primary" type="submit" data-loading-text="<i class='fa fa-spinner fa-spin'></i> WAIT..." id="send_massage" style="margin-right:6px;">SEND</button>
                      <?php }else { ?>
                      <form method="get" action="index.php">
                        <input type="hidden" name="login" value=""/>
                        <input type="submit" class="btn btn-primary" value="Log in to chat" style="margin-right:6px;"/>
                      </form>
                      <?php } ?>
                    </span> 
                  </div> 
                </div> 
              </div> 
            </div>
            <div>
              <?php if(isset($_SESSION['steamid']) && isadmin($_SESSION['steamid'])){
                echo'Admin tools: [<a href="chat/chatadm.php?do=clear" onclick="return popitup(\'chat/chatadm.php?do=clear\');">clear chat</a>], [<a href="chat/chatadm.php?do=toggle" onclick="return popitup(\'chat/chatadm.php?do=toggle\');">turn '.(chaton() ? 'off' : 'on').'</a>]';
              } 
              include ('chat/mini-chat.php'); ?>
            </div> 
          </div>
        </div>
      </div>
      <div id="roulet-content" class="row"></div>
      <div class="row">
        <div class="stuffs promo-cover">
          <ul id="game-sts">
            <div class="rounditems">
              <?php include "ajax/items.php"; ?>
            </div>
          </ul>
        </div>
      </div>
    </div>
  </div>
</body>

 <script language="javascript" type="text/javascript">
function popitup(url) {
  newwindow=window.open(url,'name','height=500,width=350');
  if (window.focus) {newwindow.focus()}
  return false;
}
</script>
</html>
